#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"
#include "common/Texture.hpp"
#include "common/LightInfo.hpp"

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <glm/gtx/rotate_vector.hpp>

#include <iostream>
#include <vector>

std::ostream& operator <<(std::ostream& out, glm::vec3 v)
{
	out << v.x << ' ' << v.y << ' ' << v.z;
	return out;
}

struct SimpleModel
{
	std::vector<glm::vec3> vertices, normals;
	std::vector<glm::vec2> texcoords;

	MeshPtr getMesh()
	{
		DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
		buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

		DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
		buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

		DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
		buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

		MeshPtr mesh = std::make_shared<Mesh>();
		mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
		mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
		mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
		mesh->setPrimitiveType(GL_TRIANGLES);
		mesh->setVertexCount(vertices.size());

		return mesh;
	}
};

struct Tree
{
	MeshPtr branches;
	MeshPtr leafs;
};

void make_leaf(SimpleModel& leafs, const glm::vec3& start, const glm::vec3& end, const glm::vec3& rotation = glm::vec3(rand(), rand(), rand()))
{
	auto direction = end - start;
	auto perpendicular = glm::normalize(glm::cross(direction, rotation));
	auto normal = glm::cross(direction, perpendicular);
	perpendicular *= 0.5f * glm::distance(start, end);

	std::vector<glm::vec3> tmpVertices;
	std::vector<glm::vec2> tmpTexcoords;

	tmpVertices.push_back(start);
	tmpVertices.push_back(end);
	tmpVertices.push_back((start + end) * 0.5f + perpendicular);
	tmpVertices.push_back((start + end) * 0.5f - perpendicular);

	tmpTexcoords.emplace_back(1.0f, 0.0f);
	tmpTexcoords.emplace_back(0.0f, 1.0f);
	tmpTexcoords.emplace_back(1.0f, 1.0f);
	tmpTexcoords.emplace_back(0.0f, 0.0f);

	auto add = [&](size_t idx)
	{
		leafs.vertices.push_back(tmpVertices[idx]);
		leafs.normals.push_back(normal);
		leafs.texcoords.push_back(tmpTexcoords[idx]);
	};

	add(0); add(1); add(2);
	add(0); add(2); add(1);
	add(0); add(1); add(3);
	add(0); add(3); add(1);
}

void make_branches(
	SimpleModel& branches, SimpleModel& leafs,
	glm::vec3 start, glm::vec3 end, float startR, float ratioR, float ratioL, int startPoints, int depth)
{
	std::vector<glm::vec3> bottom, bottomNormals;
	std::vector<glm::vec3> top, topNormals;

	float endR = startR * ratioR;
	int endPoinits = startPoints / 2;

	auto rotationMatrix = glm::orientation(glm::normalize(end - start), glm::vec3(0.0f, 0.0f, 1.0f));

	for (size_t i = 0; i < startPoints; ++i)
	{
		float theta = 2.0f * glm::pi<float>() * i / startPoints;
		glm::vec4 pt(cos(theta), sin(theta), 0.0f, 0.0f);

		bottom.push_back(glm::vec3(rotationMatrix * (pt * startR)) + start);
		bottomNormals.push_back(glm::vec3(rotationMatrix * pt));

		if (i % 2 == 0)
		{
			top.push_back(glm::vec3(rotationMatrix * (pt * endR + glm::vec4(0.0f, 0.0f, glm::distance(start, end), 0.0f))) + start);
			topNormals.push_back(glm::vec3(rotationMatrix * pt));
		}
	}

	auto addBTT = [&](size_t i, size_t j, size_t k)
	{
		branches.vertices.push_back(bottom[i % startPoints]);
		branches.vertices.push_back(top[j % endPoinits]);
		branches.vertices.push_back(top[k % endPoinits]);
		branches.normals.push_back(bottomNormals[i % startPoints]);
		branches.normals.push_back(topNormals[j % endPoinits]);
		branches.normals.push_back(topNormals[k % endPoinits]);
		branches.texcoords.emplace_back(float(i) / (startPoints - 1), 0.0f);
		branches.texcoords.emplace_back(float(j) / (endPoinits - 1), 1.0f);
		branches.texcoords.emplace_back(float(k) / (endPoinits - 1), 1.0f);
	};

	auto addBBT = [&](size_t i, size_t j, size_t k)
	{
		branches.vertices.push_back(bottom[i % startPoints]);
		branches.vertices.push_back(bottom[j % startPoints]);
		branches.vertices.push_back(top[k % endPoinits]);
		branches.normals.push_back(bottomNormals[i % startPoints]);
		branches.normals.push_back(bottomNormals[j % startPoints]);
		branches.normals.push_back(topNormals[k % endPoinits]);
		branches.texcoords.emplace_back(float(i) / (startPoints - 1), 0.0f);
		branches.texcoords.emplace_back(float(j) / (startPoints - 1), 0.0f);
		branches.texcoords.emplace_back(float(k) / (endPoinits - 1), 1.0f);
	};

	for (size_t i = 0; i < endPoinits; ++i)
	{
		addBTT(2 * i + 1, i + 1, i);
		addBBT(2 * i + 1, 2 * i + 2, i + 1);
		addBBT(2 * i + 2, 2 * i + 3, i + 1);
	}

	if (depth >= 1)
	{
		std::vector<glm::vec4> diff;
		diff.emplace_back(1.0f, 0.0f, 0.0f, 0.0f);
		diff.emplace_back(-1.0f, 0.0f, 0.0f, 0.0f);
		diff.emplace_back(0.0f, 1.0f, 0.0f, 0.0f);
		diff.emplace_back(0.0f, -1.0f, 0.0f, 0.0f);
		
		for (size_t i = 0; i < diff.size(); ++i)
		{
			glm::vec3 direction = glm::normalize(end - start + glm::vec3(rotationMatrix * diff[i]) * glm::distance(start, end));
			if (depth > 1)
			{
				make_branches(branches, leafs,
					          end, end + direction * glm::distance(start, end) * ratioL,
					          endR, ratioR, ratioL, endPoinits, depth - 1);
			}
			else
			{
				// can use (end - start) as rotation vector
				make_leaf(leafs, end, end + direction * glm::distance(start, end) * ratioL);
			}
		}
	}
}

Tree make_tree(glm::vec3 start, glm::vec3 end, float startR, float ratioR, float ratioL, int startVertices, int depth)
{
	SimpleModel branches, leafs;

	make_branches(branches, leafs, start, end, startR, ratioR, ratioL, startVertices, depth);

	return { branches.getMesh(), leafs.getMesh() };
}

class TreeApplication : public Application
{
public:
    Tree _tree;
	TexturePtr _texture, _leafTexture;

	MeshPtr _marker;

	//Идентификатор шейдерной программы
	ShaderProgramPtr _shader;
	ShaderProgramPtr _markerShader;

	//Переменные для управления положением одного источника света
	float _lr = 30.0;
	float _phi = 0.0;
	float _theta = glm::pi<float>() * 0.25f;

	LightInfo _light;

	GLuint _sampler;

    void makeScene() override
    {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

		_tree = make_tree(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), 0.125f, 0.4f, 0.6f, 512, 6);
		_marker = makeSphere(0.1f);

		//=========================================================
		//Инициализация шейдеров
		_shader = std::make_shared<ShaderProgram>("496ShcherbikData/texture.vert", "496ShcherbikData/texture.frag");
		_markerShader = std::make_shared<ShaderProgram>("496ShcherbikData/marker.vert", "496ShcherbikData/marker.frag");

		//=========================================================
		//Инициализация значений переменных освщения
		_light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
		_light.ambient = glm::vec3(0.3, 0.3, 0.3);
		_light.diffuse = glm::vec3(0.9, 0.9, 0.9);
		_light.specular = glm::vec3(0.1, 0.1, 0.1);

		//=========================================================
		//Загрузка и создание текстур
		_texture = loadTexture("496ShcherbikData/kora.png");
		_leafTexture = loadTexture("496ShcherbikData/leaf.png", SRGB::NO, 4);

		glEnable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		//=========================================================
		//Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
		glGenSamplers(1, &_sampler);
		glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Устанавливаем шейдер.
        _shader->use();

        //Устанавливаем общие юниформ-переменные
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		_light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
		glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

		_shader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
		_shader->setVec3Uniform("light.La", _light.ambient);
		_shader->setVec3Uniform("light.Ld", _light.diffuse);
		_shader->setVec3Uniform("light.Ls", _light.specular);

		// Рисуем ствол
		GLuint branchesTextureUnit = 0;
		if (USE_DSA) 
		{
			glBindTextureUnit(branchesTextureUnit, _texture->texture());
			glBindSampler(branchesTextureUnit, _sampler);
		}
		else 
		{
			glBindSampler(branchesTextureUnit, _sampler);
			glActiveTexture(GL_TEXTURE0 + branchesTextureUnit);  //текстурный юнит 0
			_texture->bind();
		}
		_shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _tree.branches->modelMatrix()))));
		
		// Рисуем листья
		GLuint leafsTextureUnit = 1;
		if (USE_DSA)
		{
			glBindTextureUnit(leafsTextureUnit, _leafTexture->texture());
			glBindSampler(leafsTextureUnit, _sampler);
		}
		else
		{
			glBindSampler(leafsTextureUnit, _sampler);
			glActiveTexture(GL_TEXTURE0 + leafsTextureUnit);  //текстурный юнит 0
			_leafTexture->bind();
		}

		_shader->setIntUniform("diffuseTex", branchesTextureUnit);
		_shader->setMat4Uniform("modelMatrix", _tree.branches->modelMatrix());
		_tree.branches->draw();
		
		glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
		glDepthFunc(GL_LESS);
		_tree.leafs->draw();
		glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
		glDepthFunc(GL_GREATER);
		_shader->setIntUniform("diffuseTex", leafsTextureUnit);
		_shader->setMat4Uniform("modelMatrix", _tree.leafs->modelMatrix());
		_tree.leafs->draw();
		
		glDepthFunc(GL_LESS);
		_shader->setIntUniform("diffuseTex", branchesTextureUnit);
		_shader->setMat4Uniform("modelMatrix", _tree.branches->modelMatrix());
		_tree.branches->draw();

		glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
		glDepthFunc(GL_LESS);
		_tree.leafs->draw();
		_tree.branches->draw();
		glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
		glDepthFunc(GL_LEQUAL);
		_shader->setIntUniform("diffuseTex", leafsTextureUnit);
		_shader->setMat4Uniform("modelMatrix", _tree.leafs->modelMatrix());
		_tree.leafs->draw();

		_markerShader->use();
		_markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
		_markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
		_marker->draw();

		glBindSampler(0, 0);
		glUseProgram(0);
    }
};

int main()
{
    TreeApplication app;
    app.start();

    return 0;
}