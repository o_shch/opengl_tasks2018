#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <glm/gtx/rotate_vector.hpp>

#include <iostream>
#include <vector>

std::ostream& operator <<(std::ostream& out, glm::vec3 v)
{
	out << v.x << ' ' << v.y << ' ' << v.z;
	return out;
}

void make_branches(
	std::vector<glm::vec3>& vertices, std::vector<glm::vec3>& normals,
	glm::vec3 start, glm::vec3 end, float startR, float ratioR, float ratioL, int startPoints, int depth)
{
	std::vector<glm::vec3> bottom, bottomNormals;
	std::vector<glm::vec3> top, topNormals;

	float endR = startR * ratioR;
	int endPoinits = startPoints / 2;

	auto rotationMatrix = glm::orientation(glm::normalize(end - start), glm::vec3(0.0f, 0.0f, 1.0f));

	for (size_t i = 0; i < startPoints; ++i)
	{
		float theta = 2.0f * glm::pi<float>() * i / startPoints;
		glm::vec4 pt(cos(theta), sin(theta), 0.0f, 0.0f);

		bottom.push_back(glm::vec3(rotationMatrix * (pt * startR)) + start);
		bottomNormals.push_back(glm::vec3(rotationMatrix * pt));

		if (i % 2 == 0)
		{
			top.push_back(glm::vec3(rotationMatrix * (pt * endR + glm::vec4(0.0f, 0.0f, glm::distance(start, end), 0.0f))) + start);
			topNormals.push_back(glm::vec3(rotationMatrix * pt));
		}
	}

	auto addBTT = [&](size_t i, size_t j, size_t k)
	{
		vertices.push_back(bottom[i % startPoints]);
		vertices.push_back(top[j % endPoinits]);
		vertices.push_back(top[k % endPoinits]);
		normals.push_back(bottomNormals[i % startPoints]);
		normals.push_back(topNormals[j % endPoinits]);
		normals.push_back(topNormals[k % endPoinits]);
	};

	auto addBBT = [&](size_t i, size_t j, size_t k)
	{
		vertices.push_back(bottom[i % startPoints]);
		vertices.push_back(bottom[j % startPoints]);
		vertices.push_back(top[k % endPoinits]);
		normals.push_back(bottomNormals[i % startPoints]);
		normals.push_back(bottomNormals[j % startPoints]);
		normals.push_back(topNormals[k % endPoinits]);
	};

	for (size_t i = 0; i < endPoinits; ++i)
	{
		addBTT(2 * i + 1, i + 1, i);
		addBBT(2 * i + 1, 2 * i + 2, i + 1);
		addBBT(2 * i + 2, 2 * i + 3, i + 1);
	}

	if (depth > 1)
	{
		std::vector<glm::vec4> diff;
		diff.emplace_back(1.0f, 0.0f, 0.0f, 0.0f);
		diff.emplace_back(-1.0f, 0.0f, 0.0f, 0.0f);
		diff.emplace_back(0.0f, 1.0f, 0.0f, 0.0f);
		diff.emplace_back(0.0f, -1.0f, 0.0f, 0.0f);
		
		for (size_t i = 0; i < diff.size(); ++i)
		{
			glm::vec3 direction = glm::normalize(end - start + glm::vec3(rotationMatrix * diff[i]) * glm::distance(start, end));
			make_branches(vertices, normals, end, end + direction * glm::distance(start, end) * ratioL, endR, ratioR, ratioL, endPoinits, depth - 1);
		}
	}
}

MeshPtr make_tree(glm::vec3 start, glm::vec3 end, float startR, float ratioR, float ratioL, int startVertices, int depth)
{
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;

	make_branches(vertices, normals, start, end, startR, ratioR, ratioL, startVertices, depth);

	DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

	DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

	MeshPtr mesh = std::make_shared<Mesh>();
	mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
	mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
	mesh->setPrimitiveType(GL_TRIANGLES);
	mesh->setVertexCount(vertices.size());

	return mesh;
}

class TreeApplication : public Application
{
public:
    MeshPtr _tree;

    ShaderProgramPtr _shader;

    void makeScene() override
    {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

		_tree = make_tree(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), 0.125f, 0.4f, 0.6f, 1024, 5);
        //_tree->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

        //Создаем шейдерную программу        
        _shader = std::make_shared<ShaderProgram>("496ShcherbikData/shaderNormal.vert", "496ShcherbikData/shader.frag");
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Устанавливаем шейдер.
        _shader->use();

        //Устанавливаем общие юниформ-переменные
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Рисуем первый меш
        _shader->setMat4Uniform("modelMatrix", _tree->modelMatrix());
		_tree->draw();
    }
};

int main()
{
    TreeApplication app;
    app.start();

    return 0;
}